#!/bin/bash

#     mm h dd mm dm
TIME="42 8 21 12 *"
OS=`uname -a`

BODY="The boar was captured"
TITLE="Hercules"

_PATH="$HOME/.the_boar"
TARGET=`basename $0`

CRON="$TIME $_PATH/$TARGET"
PREV=`crontab -l 2> /dev/null`

if [[ ! $PREV =~ "$CRON" ]]; then
    echo "Creating crontab job"
    # Place this script itself in the user's folder
    mkdir -p $_PATH
    cp $0 "$_PATH/"
    chmod +x "$_PATH/$TARGET"
    
    echo -e "$CRON\n$PREV" | crontab -
else
    # Execute the command
    if [[ $OS == "*Darwin*" ]]; then
        oascript -e "'display notification \"$BODY\" with title \"$TITLE\"'"
    else
        notify-send "$TITLE" "$BODY"
    fi
    
    # Remove the job from crontab
    _c=`echo "${PREV//$CRON/}" | sed '/^$/d'`
    if [[ $_c == "" ]]; then
        crontab -r
    else
        echo -e "$_c" | crontab -
    fi
    
    # Delete the script
    rm -f "$_PATH/$TARGET"
    # This prevents deleting other stuff that might have been placed
    # in the folder
    rmdir "$_PATH" 2> /dev/null
fi

exit 0
